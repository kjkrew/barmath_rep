Reputation Bar Addon v<%version%>, for BarMath 2.x


-= Troubleshooting =-

Q: Help! My Reputation bar keeps switching my watched rep!
A: This is a feature, not a bug. You can turn this off by opening the BarMath Options window, clicking on the Reputation line on the left, opening the Tracking tab and turning off the AutoTrack feature.

Q: I did an Aldor/Scryer turn-in, and now my Reputation Bar is watching the wrong rep!
A: This is a known issue, that I have yet to figure out a way to fix. Version 1.2 will feature a way to only track positive rep, which should bypass this issue.

Q: Why isn't my Reputation bar auto-switching to (Reputation Name)?
A1: Make sure your AutoTrack feature is on.
A2: There are many places that I didn't include, such as Stormwind or Orgrimmar, because I didn't feel it was necessary.
A3: I haven't had much of a chance to fully explore the new expansion, so the AutoTrack feature currently does not support Northrend factions.


-= To Do List =-
 - Add multiple Reputation Tracking options, such as only tracking positive rep only, negative rep only, Zone Switching or the default "Whatever I got last" setting.
 - Add a feature to estimate the number of reputation gains needed to level. This could be set to Quests, Mobs, or Both. You would even be able to choose how many Mobs, or Quests to average the estimation from.
 - Add an option to not track specific reputations. This option would be available in your Reputation window.

-= Version History =-
v<%version%>
 - Switched from localization system, to ID system. This should make RepBar usable in any locale!

v1.3
 - Cataclysm 4.2 update.
 - Fixed Guild Rep Bug.
 - Removed Zul'Gurub from the Auto-Set Rep fuction.
 - Firelands Rep installed.
 - Fixed a bug that wouldn't change the bar-height if the Rep bar was hidden.


v1.2
 - Added support for Icecrown Citadel & the Ashen Verdict.
 - Added support for commas in the numbers.


v1.1
 - Added the Black Temple to the auto-zone detection.
 - Fixed a bug that wouldn't load the proper reputation for Hyjal.
 - Fixed a bug for The Molten Core.
 - Added Dalaran, Level 80 Dungeons and heroics to the AutoZone function.


v1.0
 - Updated to BarMath 2.0
 - AutoTracking feature installed! This will automatically change your rep when entering an instance, or going into specified zones. (No Lich King Support yet.)