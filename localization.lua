--[[
	BarMath 2.x Addon: Reputation Bar
	English Localization
	Version <%version%>
	
	Revision: $Id: localization.lua 11 2012-12-23 11:56:42 PST Kjasi $
]]

BARMATH_REP_TITLE = "Reputation"

-- Display Options
BARMATH_DISPLAYOPTION_REP_BAR_PERBAR = "Experience Per Bar"
BARMATH_DISPLAYOPTION_REP_BAR_TOLVL = "Reputation Needed to Level"
BARMATH_DISPLAYOPTION_REP_PERC_PERBAR = "Reputation Per Percent"
BARMATH_DISPLAYOPTION_REP_LEVEL = "Current Reputation Level"

-- Text that appears on the Bars

BARMATH_BARTXT_REP_NEEDED = "Rep Needed: "
BARMATH_BARTXT_REP_NEEDED_TO_LEVEL = "Reputation Needed to Level: "
BARMATH_BARTXT_REP_PER_P = "Rep Per %: "
BARMATH_BARTXT_REP_PER_PERCENT = "Reputation Per Percent: "
BARMATH_BARTXT_REPUTATION_LEVEL = "Reputation Level: "
BARMATH_BARTXT_REP_LEVEL = "Rep Level: "
BARMATH_BARTXT_PERBAR_REP = "Reputation Per Bar: "

-- Long Text
BARMATH_BARTXT_REP_BARS_FILLED = "Bars Filled: "
BARMATH_BARTXT_REP_PERBAR = "Reputation Per Bar: "
BARMATH_BARTXT_REP_NEEDED_TO_LEVEL = "Reputation Needed to Level: "
BARMATH_BARTXT_REP_BARS_TO_LEVEL = "Bars Needed to Level: "
BARMATH_BARTXT_REP_PERCENT_EARNED = "Percent Earned: "
BARMATH_BARTXT_REP_PERCENT_TO_LEVEL = "Percent Needed to Level: "
BARMATH_BARTXT_REP_PER_PERCENT = "Reputation Per Percent: "
BARMATH_BARTXT_REP_LEVEL = "Reputation Level: "

-- Short Text
BARMATH_BARTXT_REP_S_BARS_FILLED = "Bars: "
BARMATH_BARTXT_REP_S_PERBAR = "Rep Per Bar: "
BARMATH_BARTXT_REP_S_NEEDED_TO_LEVEL = "Rep Needed: "
BARMATH_BARTXT_REP_S_BARS_TO_LEVEL = "Bars Needed: "
BARMATH_BARTXT_REP_S_PERCENT_EARNED = "Percent: "
BARMATH_BARTXT_REP_S_PERCENT_TO_LEVEL = "% To Level: "
BARMATH_BARTXT_REP_S_PER_PERCENT = "Rep Per Percent: "
BARMATH_BARTXT_REP_S_LEVEL = "Rep Level: "

-- Tooltips
BARMATH_REP_TOOLTIP_AUTOTRACKENABLETITLE = "Enable AutoTracking"
BARMATH_REP_TOOLTIP_AUTOTRACKDISABLETITLE = "Disable AutoTracking"
BARMATH_REP_TOOLTIP_AUTOTRACKTEXT = "The AutoTracking feature will automatically\nchange your reputation to the appropriate\nfaction upon entering an instance, or a\nparticular zone."

--[[ Factions ]]
BARMATH_REP_INACTIVE = "Inactive"
BARMATH_REP_FACTION_GUILD = "Guild"