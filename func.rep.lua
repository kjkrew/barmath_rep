--[[
	BarMath 2.x Addon: Reputation Bar
	Reputation Functions
	Version <%version%>
	
	Revision: $Id: func.rep.lua 12 2012-12-23 11:55:44 PST Kjasi $
]]

local BMREP_Version = "<%version%>"

-- These Rep Defaults are the minimum default requirements for a BarMath Bar! These MUST be listed first!
BarMath_Defaults_Rep = {
	["Display"] = 1,			-- 1 = On, 0 = Off
	["DisplayMethod"] = 0,			-- 1 = Text is Always on, 0 = Text only when mouseover
	["TextLength"] = BARMATH_TEXT_SHORT,	-- Other Value: BARMATH_TEXT_LONG
	["AutoTrack"] = 1, 			-- 1 = On, 0 = Off
	["DontWatch"] = {},
	["Displays"] = {
		["Display1"] = 1,		-- Determines the default option for the Displays.
		["Display2"] = 9,
		["Display3"] = 3,
		["Display4"] = 4,
		["Display5"] = 5,
		["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
		["Display2OnOff"] = 1,
		["Display3OnOff"] = 1,
		["Display4OnOff"] = 1,
		["Display5OnOff"] = 1,				
	},
}

-- Add Bar to Barlist
	-- First Variable is the Bar's Name. With our XP example, the final name for the bar will be "BarMathXPBar".
	-- Second is the update function. This is called to update the bar's data.
	-- Third is the default option settings for this bar.
	-- Fourth variable is the parent. If not set, or the bar doesn't exist, then BarMath will automatically assign a parent.
	-- Fifth variable is the InfoDelete function that should be called when deleting a character's information from the Info Database.
	-- Sixth is the template this bar should follow. This is useful for if you want to make your own bar template, say with 4 bars instead of 1.
BarMath_AddBar("Rep", "BarMath_Rep_Update", BarMath_Defaults_Rep)


-- Option Text
-- The following is Option Text. All of them MUST be in an identical order!

-- Dropdown Text
BarMath_Option_DropDown_Rep_Listing = {
	"BARMATH_DISPLAYOPTION_BAR_FILLED", 		-- Amount of Bars Filled
	"BARMATH_DISPLAYOPTION_REP_BAR_PERBAR",		-- Reputation Per Bar
	"BARMATH_DISPLAYOPTION_CURRMAX",		-- The Current / The Maximum
	"BARMATH_DISPLAYOPTION_BAR_TOLVL",		-- Amount needed to Level (Max - Current)
	"BARMATH_DISPLAYOPTION_BAR_TOFILL",		-- Bars needed to Level
	"BARMATH_DISPLAYOPTION_PERC_FILLED",		-- Percent Filled
	"BARMATH_DISPLAYOPTION_PERC_TOFILL",		-- Percent Needed to Level
	"BARMATH_DISPLAYOPTION_REP_PERC_PERBAR",	-- Reputation Per Percent
	"BARMATH_DISPLAYOPTION_REP_LEVEL",		-- Current Reputation Level (IE: Friendly)
}

-- Long Text
BARMATH_TXT_REP_LONG = {
	BARMATH_BARTXT_REP_BARS_FILLED,			-- BARMATH_DISPLAYOPTION_BAR_FILLED
	BARMATH_BARTXT_REP_PERBAR,			-- BARMATH_DISPLAYOPTION_BAR_PERBAR
	BARMATH_BARTXT_REP_NEEDED_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOLVL
	BARMATH_BARTXT_REP_BARS_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOFILL
	BARMATH_BARTXT_REP_PERCENT_EARNED,		-- BARMATH_DISPLAYOPTION_PERC_FILLED
	BARMATH_BARTXT_REP_PERCENT_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_PERC_TOFILL
	BARMATH_BARTXT_REP_PER_PERCENT,			-- BARMATH_DISPLAYOPTION_REP_PERC_PERBAR
	BARMATH_BARTXT_REP_LEVEL,			-- BARMATH_DISPLAYOPTION_REP_LEVEL
}

BARMATH_TXT_REP_SHORT = {
	BARMATH_BARTXT_REP_S_BARS_FILLED,		-- BARMATH_DISPLAYOPTION_BAR_FILLED
	BARMATH_BARTXT_REP_S_PERBAR,			-- BARMATH_DISPLAYOPTION_BAR_PERBAR
	BARMATH_BARTXT_REP_S_NEEDED_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOLVL
	BARMATH_BARTXT_REP_S_BARS_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOFILL
	BARMATH_BARTXT_REP_S_PERCENT_EARNED,		-- BARMATH_DISPLAYOPTION_PERC_FILLED
	BARMATH_BARTXT_REP_S_PERCENT_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_PERC_TOFILL
	BARMATH_BARTXT_REP_S_PER_PERCENT,		-- BARMATH_DISPLAYOPTION_REP_PERC_PERBAR
	BARMATH_BARTXT_REP_S_LEVEL,			-- BARMATH_DISPLAYOPTION_REP_LEVEL
}

function BarMath_Rep_Update(newLevel,faction)
	if (not newLevel) then
		newLevel = UnitLevel("player")
	end
	if (BarMath_HaveLoaded["Bars"]["Rep"]) then
		-- Hide the original Rep Bar!
		ReputationWatchBar:Hide()

		if (not faction) then
			faction = GetWatchedFactionInfo()
		end

		if (faction == nil) then
			BarMathRepBar:Hide()
		else
			BarMathRepBar:Show()
		end

		if (not BarMathRepBar:IsVisible()) then
			-- Run Show/Hide to finalize any changes
			BarMath_Bar_ShowHide("Rep")
			return
		end

		local AutoTrack = BarMath_GetCharVar("Bars", "RepBar", "AutoTrack")

		if (AutoTrack == 1) then
			-- BarMath_Msg("Autotrack On.")
			-- BarMath_Msg("Pre-Guild Faction = "..tostring(faction)..", BMGuild:"..BARMATH_REP_FACTION_GUILD..", isInGuild:"..tostring(IsInGuild()))
			if (faction == BARMATH_REP_FACTION_GUILD) and (IsInGuild()) then
				faction = GetGuildInfo("player")
				-- BarMath_Msg("Guild name = "..tostring(guildname))
			end
			-- BarMath_Msg("Post-Guild Faction = "..tostring(faction))

			for x=1, GetNumFactions() do
				local name,_,_,_,_,_,_,_,isHeader,_,isWatched = GetFactionInfo(x)
				--if isHeader == nil then
					if isWatched == nil then
						if name == faction then
							SetWatchedFactionIndex(x)
						end
					end
				--end
			end
		end

		-- BarMath_Msg("Processing Faction: "..faction..", AutoTrack: "..AutoTrack)

		-- Dah Math!
		local name, reaction, minimum, maximum, value = GetWatchedFactionInfo()
		if (not name) then
			return
		end
		if name ~= faction then

		end
		local perbar, bars, togo, XPtogo, pcent, pcent_togo = BarMath_Rep_Calculate()

		local option = {}
		local display = {}
		local post
		local repperpcent = BarMath_Round((minimum/100),BarMath_GetCharVar("Rounding"),BarMath_GetCharVar("Decimals"))

		-- Determine what text to use if the text length is short or long.
		if (BarMath_GetCharVar("Bars", "RepBar", "TextLength") == BARMATH_TEXT_SHORT) then
			txt = BARMATH_TXT_REP_SHORT
		else
			txt = BARMATH_TXT_REP_LONG
		end

		local replvl = _G["FACTION_STANDING_LABEL"..reaction]

		option[1] = txt[1]..BarMath_AddCommas(bars)
		option[2] = txt[2]..BarMath_AddCommas(perbar)
		option[3] = name.." "..BarMath_AddCommas(value-minimum).." / "..BarMath_AddCommas(maximum-minimum)
		option[4] = txt[3]..BarMath_AddCommas(XPtogo)
		option[5] = txt[4]..BarMath_AddCommas(togo)
		option[6] = txt[5]..BarMath_AddCommas(pcent).."%"
		option[7] = txt[6]..BarMath_AddCommas(pcent_togo).."%"
		option[8] = txt[7]..BarMath_AddCommas(repperpcent)
		option[9] = txt[8]..replvl

		-- Determine which options need to be displayed.
		for i=1, BarMath.DisplayCount, 1 do
			local thisopt = BarMath_GetCharVar("Bars","RepBar","Displays","Display"..i)
			display[i] = tostring(option[thisopt])
		end

		-- Set the Displays!
		for i=1, BarMath.DisplayCount, 1 do
			BarMath_SetBarText(display[i], "Rep", i)
		end

		-- Set the Bar!
		local color = FACTION_BAR_COLORS[reaction]
		BarMathRepBar:SetStatusBarColor(color.r, color.g, color.b)
		BarMathRepBar:SetMinMaxValues(min(0, value-minimum), maximum-minimum)
		BarMathRepBar:SetValue(value-minimum)

		-- Run Show/Hide to finalize any changes
		BarMath_Bar_ShowHide("Rep")
	end
end

function BarMath_Faction_Update(repline)
	if (not BarMathRepBar:IsVisible()) then
		return
	end

	local start, faction, amount, standingId, isWatched
	local Check_Var = {
		string.gsub(string.gsub(FACTION_STANDING_DECREASED, "(%%s)", "(.+)"), "(%%d)", "(.+)"),
		string.gsub(string.gsub(FACTION_STANDING_INCREASED, "(%%s)", "(.+)"), "(%%d)", "(.+)"),
		string.gsub(FACTION_STANDING_DECREASED_GENERIC, "(%%s)", "(.+)"),
		string.gsub(FACTION_STANDING_INCREASED_GENERIC, "(%%s)", "(.+)"),
		string.gsub(FACTION_STANDING_CHANGED, "(%%s)", "(.+)"),
	}

	for x=1, getn(Check_Var) do
		if string.find(repline, Check_Var[x]) then
			start, _, faction, amount = string.find(repline, Check_Var[x])
			if start then
				local thefaction
				for r=1, GetNumFactions() do
					local name = GetFactionInfo(r)
					if faction == name then
						thefaction = r
						break
					end
				end
				if thefaction then
					_, _, standingId, _, _, _, _, _, _, _, isWatched = GetFactionInfo(thefaction)
					break
				end
			end
		end
	end
	if (not standingId) then
		return
	end
	if (standingId ~= 8) and (standingId ~= 0) then
		if (isWatched ~= 1) then
			BarMath_Rep_Update(nil,faction)
		end
	end
end

function BarMath_Rep_Calculate()
	local perbar, bars, togo, phave, pneed, current, max, max2

	local _, _, min2, max2, current2 = GetWatchedFactionInfo()
	current = current2-min2
	max = max2-min2
	
	perbar = BarMath_Round(max/BarMath.NumBars)
	XPTogo = max - current

	local amount = (current/max)*100
	phave = BarMath_Round(amount)
	pneed = BarMath_Round(100-phave)
	bars = BarMath_Round(current/perbar)
	togo = BarMath_Round(BarMath.NumBars-bars)

	return perbar, bars, togo, XPTogo, phave, pneed
end

function BarMath_Rep_ZoneChange()
	local AutoTrack = BarMath_GetCharVar("Bars", "RepBar", "AutoTrack")
	if (AutoTrack ~= 1) then
		return
	end
	SetMapToCurrentZone()
	--print("Zone Change Running...")

	local ininstance, instancetype = IsInInstance()
	local zoneID = GetCurrentMapAreaID()
	local Zonename = GetZoneText()
	
	--print("Current ZoneID: ["..zoneID.."], Name: ["..Zonename.."]")

	--[[ Outdoor Zones ]]
	if ((zoneID == 261) or (zoneID == 241)) then
		BarMath_Rep_SetRepID(609)			-- Cenarian Circle
	elseif ((zoneID == 22) or (zoneID == 23)) then
		BarMath_Rep_SetRepID(1106)
	elseif (zoneID == 465) then
		BarMath_Rep_SetRepID(946)
		BarMath_Rep_SetRepID(947)
	elseif (zoneID == 499) then
		BarMath_Rep_SetRepID(1077)
	elseif (zoneID == 510) then
		BarMath_Rep_SetRepID(1090)
		
	--[[ PvP Zones ]]
	elseif (zoneID == 443) then
		BarMath_Rep_SetRepID(890)
		BarMath_Rep_SetRepID(889)
	elseif (zoneID == 461) then
		BarMath_Rep_SetRepID(509)
		BarMath_Rep_SetRepID(510)
	elseif (Zonename == BARMATH_REP_PVP_AV) then
		BarMath_Rep_SetRepID(730)
		BarMath_Rep_SetRepID(729)
		
	--[[ Instance Zones & Raids]]
	-- Ahn'Qiraj 20
	elseif (zoneID == 717) then
		BarMath_Rep_SetRepID(609)			-- Cenarian Circle
	-- Ahn'Qiraj 40
	elseif (zoneID == 766) then
		BarMath_Rep_SetRepID(910)
	-- Molten Core
	elseif (zoneID == 696) then
		BarMath_Rep_SetRepID(749)
	-- Hellfire Citadel
	elseif ((zoneID == 797)or(zoneID == 725)or(zoneID == 710)) then
		BarMath_Rep_SetRepID(946)
		BarMath_Rep_SetRepID(947)
	-- Tempest Keep
	elseif ((zoneID == 729)or(zoneID == 730)or(zoneID == 731)) then
		BarMath_Rep_SetRepID(935)
	-- Caverns of Time
	elseif ((zoneID == 734)or(zoneID == 733)) then
		BarMath_Rep_SetRepID(989)
	elseif (Zonename == 775) then
		BarMath_Rep_SetRepID(990)
	-- Coilfang Reservoir
	elseif ((zoneID == 728)or(zoneID == 727)or(zoneID == 726)) then
		BarMath_Rep_SetRepID(942)
	-- Auchindoun
	elseif (zoneID == 732) then
		BarMath_Rep_SetRepID(933)
	elseif ((zoneID == 722)or(zoneID == 723)or(zoneID == 724)) then
		BarMath_Rep_SetRepID(1011)
	-- Karazhan
	elseif (zoneID == 799) then
		BarMath_Rep_SetRepID(967)
	-- Magister's Terrace
	elseif (zoneID == 798) then
		BarMath_Rep_SetRepID(1077)
	-- Black Temple
	elseif (zoneID == 796) then
		BarMath_Rep_SetRepID(1012)
	-- Icecrown Citadel
	elseif (zoneID == 604) then
		BarMath_Rep_SetRepID(1156)
	-- Firelands
	elseif (zoneID == 800) then
		BarMath_Rep_SetRepID(1204)
		
	--[[ Tabards ]]
	elseif (ininstance == 1) and (instancetype=="party") then
		BarMath_Rep_SetTabardRep()
	end
end

function BarMath_Rep_SetWatched(index)
	if (index == 0) then
		BarMathRepBar:Hide()
	else
		BarMathRepBar:Show()
	end
	BarMath_ParentBar_SetPos()
end

local function BarMath_Rep_GetFactionIndexbyID(ID)
	local name, description, standingID, barMin, barMax, barValue, atWarWith, canToggleAtWar, isHeader, isCollapsed, hasRep, isWatched, isChild = GetFactionInfoByID(ID)
	local index
	
	-- Open any closed categories
	maxxed = 0
	while (maxxed < 30) do
		for i=1,GetNumFactions() do
			local hname,_,_,_,_,_,_,_,hisHeader,hisCollapsed,_,_,_ = GetFactionInfo(i)
			if (hisHeader) and (hname ~= BARMATH_REP_INACTIVE) then
				if (hisCollapsed) then
					ExpandFactionHeader(i)
				end
			end
		end
		maxxed = maxxed + 1
	end
	
	for i=1,GetNumFactions() do
		local fname,_,_,_,_,_,_,_,fisHeader,_,_,_,_ = GetFactionInfo(i)
		if (not fisHeader) then
			if (fname == name) then
				return i
			end
		end
	end
end

function BarMath_Rep_SetRepID(ID)
	--print("SetRepID, ID: ["..ID.."]")
	local index = BarMath_Rep_GetFactionIndexbyID(ID)
	--print("SetRepID, Index: "..tostring(index))
	if (not index) then return end

	SetWatchedFactionIndex(index)
end

function BarMath_Rep_SetTabardRep()
	local tabardslot = GetInventorySlotInfo("TabardSlot")
	local tabardlink = GetInventoryItemLink("player",tabardslot)
	local tabardid = 0
	local zoneID = GetCurrentMapAreaID()

	if tabardlink == nil then
		return
	end
	
	local justItemId = string.gsub(tabardlink,".-\124H([^\124]*)\124h.*", "%1")
	local type, itemid, enchantId, jewelId1, jewelId2, jewelId3, jewelId4, suffixId, uniqueId = strsplit(":",justItemId)
	tabardid = tonumber(itemid)

	-- Guild Tabards
	if (tabardid == 45574) then
		BarMath_Rep_SetRepID(72)
	elseif (tabardid == 45577) then
		BarMath_Rep_SetRepID(47)
	elseif (tabardid == 45578) then
		BarMath_Rep_SetRepID(54)
	elseif (tabardid == 45579) then
		BarMath_Rep_SetRepID(69)
	elseif (tabardid == 45580) then
		BarMath_Rep_SetRepID(930)
	elseif (tabardid == 45581) then
		BarMath_Rep_SetRepID(76)
	elseif (tabardid == 45582) then
		BarMath_Rep_SetRepID(530)
	elseif (tabardid == 45583) then
		BarMath_Rep_SetRepID(68)
	elseif (tabardid == 45584) then
		BarMath_Rep_SetRepID(81)
	elseif (tabardid == 45585) then
		BarMath_Rep_SetRepID(911)
	elseif (tabardid == 64882) then
		BarMath_Rep_SetRepID(1134)
	elseif (tabardid == 64884) then
		BarMath_Rep_SetRepID(1133)
	-- Northrend Tabards
	elseif (tabardid == 43154) then
		BarMath_Rep_SetRepID(1106)
	elseif (tabardid == 43155) then
		BarMath_Rep_SetRepID(1098)
	elseif (tabardid == 43156) then
		BarMath_Rep_SetRepID(1091)
	elseif (tabardid == 43157) then
		BarMath_Rep_SetRepID(1090)
	-- Cataclysm Tabards
	elseif (tabardid == 65904) then
		BarMath_Rep_SetRepID(1173)
	elseif (tabardid == 65905) then
		BarMath_Rep_SetRepID(1135)
	elseif (tabardid == 65906) then
		BarMath_Rep_SetRepID(1158)
	elseif (tabardid == 65907) then
		BarMath_Rep_SetRepID(1171)
	elseif (tabardid == 65908) then
		BarMath_Rep_SetRepID(1174)
	elseif (tabardid == 65909) then
		BarMath_Rep_SetRepID(1172)
	end
end

function BarMath_Rep_Events(self,event,...)
	
end

--== Rep Bar Listing Functions ==--

--[[ Option Window Data ]]--

-- Option Window information
function BarMath_Rep_Options()
	-- Used for listing tabs & their templates.
	local tabs = {
		[1] = {
			["TabTitle"] = "Tracking",
			["TabTemp"] = "BarMath_Rep_Tab_Tracking",
		},
	}
	local BarTitle = "Rep" 	-- Used internally. Doesn't need localization.
	local displayname = BARMATH_REP_TITLE -- The name of the Addon. Used in the Options list.
	BarMath_Generate_Options(BarTitle,displayname,BarMath_Option_DropDown_Rep_Listing,tabs)
end

-- Load Option Window information!
BarMath_Rep_Options()

--[[ Function Hooks ]]--
hooksecurefunc("SetWatchedFactionIndex", BarMath_Rep_SetWatched)
hooksecurefunc("ReputationWatchBar_Update", BarMath_Rep_Update)
BarMath_AddEvent("ZONE_CHANGED_NEW_AREA",BarMath_Rep_ZoneChange)
BarMath_AddEvent("PLAYER_ENTERING_WORLD",BarMath_Rep_ZoneChange)
BarMath_AddEvent("CHAT_MSG_COMBAT_FACTION_CHANGE",BarMath_Faction_Update)


-- Loaded Message
-- Comes last to ensure that everything has been loaded first!
BarMath_HaveLoaded["Addons"]["Rep"] = BARMATH_REP_TITLE.." Addon v"..BMREP_Version.." Loaded!"

-- Post-Load functions to run
BarMath_Rep_ZoneChange()