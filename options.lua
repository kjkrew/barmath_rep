--[[
	BarMath 2.x Addon: Reputation Bar
	Options Functions
	Version <%version%>
	
	Revision: $Id: options.lua 9 2012-12-23 11:57:38 PST Kjasi $
]]

function BarMath_Rep_OnLoad()
	-- Tooltips
	if (BarMath_GetCharVar("Bars","RepBar","AutoTrack") == 0) then
		BarMath_RepOptions_AutoTrackingTooltipTitle = BARMATH_REP_TOOLTIP_AUTOTRACKENABLETITLE;
	else
		BarMath_RepOptions_AutoTrackingTooltipTitle = BARMATH_REP_TOOLTIP_AUTOTRACKDISABLETITLE;
	end
	BarMath_RepOptions_AutoTrackingTooltipText = BARMATH_REP_TOOLTIP_AUTOTRACKTEXT;

	BarMath_RepOptions_AutotrackingCheckBox:SetChecked(BarMath_GetCharVar("Bars","RepBar","AutoTrack"));
end

function BarMath_RepOptions_CheckboxOnOff(checkbox, value)
	if (value == nil) then
		value = 0;
	end

	-- BarMath_Msg("Checkbox: "..tostring(checkbox)..", value: "..value);

	if checkbox == "BarMath_RepOptions_Autotracking" then
		BarMath_SetCharVar(value,"Bars","RepBar","AutoTrack");
		if (value == 0) then
			BarMath_RepOptions_AutoTrackingTooltipTitle = BARMATH_REP_TOOLTIP_AUTOTRACKENABLETITLE;
		else
			BarMath_RepOptions_AutoTrackingTooltipTitle = BARMATH_REP_TOOLTIP_AUTOTRACKDISABLETITLE;
		end
		BarMath_Tooltip_Generate(BarMath_RepOptions_Autotracking,BarMath_RepOptions_AutoTrackingTooltipTitle,BarMath_RepOptions_AutoTrackingTooltipText)
	end

	BarMath_Rep_Update();
end