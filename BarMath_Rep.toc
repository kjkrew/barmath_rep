## Title: BarMath [|CFF5e88ffReputation|r]
## Interface: 50100
## Version: <%version%>
## DefaultState: Enabled
## Author: Kjasi
## Notes: Reputation Addon for BarMath
## Dependancies: BarMath
## eMail: sephiroth3d@gmail.com
## URL: http://code.google.com/p/kjasiwowaddons/
localization.lua
func.rep.lua
options.lua
options.xml